import React from 'react';
import Route from 'react-router/lib/Route';

import App      from './components/App';
import Greeting from './components/Greeting';

export default (
  <Route path="/" component={App}>
    <Route path="greet" component={Greeting} />
  </Route>
);
