export default (state = [
  { index:  1, title: 'Javascript: The Good Parts', pages: 123 },
  { index:  2, title: 'Harry Potter',               pages: 564 },
  { index:  3, title: 'The Dark Tower',             pages: 234 },
  { index:  4, title: 'Eloquent Ruby',              pages: 105 },
  { index:  5, title: 'Learning React',             pages: 125 },
  { index:  6, title: 'Learning Webpack',           pages: 896 }
], action) => {
  switch (action.type) {
    case 'BOOK_ADDED':
      return state.concat(action.payload);
    default:
      return state;
  }
};
