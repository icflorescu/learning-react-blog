import React                            from 'react';
import ReactDOM                         from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider }                     from 'react-redux';
import Router                           from 'react-router/lib/Router';
import browserHistory                   from 'react-router/lib/browserHistory';

import reducers from './reducers';
import routes   from './routes';

import './index.scss';

const store = applyMiddleware()(createStore)(reducers);
const Root = (
  <Provider store={store}>
    <Router history={browserHistory} routes={routes} />
  </Provider>
);
const rootEl = document.getElementById('app');

if (process.env.NODE_ENV == 'production') {
  ReactDOM.render(Root, rootEl);
} else {
  const AppContainer = require('react-hot-loader/lib/AppContainer');
  const render = () => {
    ReactDOM.render(<AppContainer>{Root}</AppContainer>, rootEl);
  };
  if (module.hot) {
    module.hot.accept('./routes', render);
    module.hot.accept('./reducers', () => { store.replaceReducer(reducers); });
  }
  render();
}
