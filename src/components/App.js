import React from 'react';
import Link  from 'react-router/lib/Link';

import Title       from './Title';
import BookList    from '../containers/BookList';
import BookDetails from '../containers/BookDetails';

export default props => (
  <div>
    <Title />
    <BookList />
    <hr />
    <BookDetails />
    <p>Click <Link to="greet">here</Link> to test router.</p>
    {props.children}
  </div>
);
